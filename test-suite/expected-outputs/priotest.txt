RODOS RODOS-200.0 OS Version Linux-x86
Loaded Applications:
         10 -> 'Topics & Middleware'
       3000 -> 'priorityTest'
Calling Initiators and Application Initiators
Distribute Subscribers to Topics
List of Middleware Topics:
 CharInput  Id = 28449 len = 16.   -- Subscribers:
 SigTermInterrupt  Id = 16716 len = 4.   -- Subscribers:
 UartInterrupt  Id = 15678 len = 4.   -- Subscribers:
 TimerInterrupt  Id = 25697 len = 4.   -- Subscribers:
 routerTopic  Id = 21324 len = 1326.   -- Subscribers:
 gatewayTopic  Id = 0 len = 16.   -- Subscribers:

Event servers:
Threads in System:
   Prio =       0 Stack =  32000 IdleThread: yields all the time
   Prio =      30 Stack =  32000 LowPriority:  lopri
   Prio =      30 Stack =  32000 LowPriority:  lopri
   Prio =     100 Stack =  32000 HiPriority:  hipri
   Prio =     100 Stack =  32000 HiPriority:  hipri
BigEndianity = 0, cpu-Arc = x86, Basis-Os = baremetal, Cpu-Speed (K-Loops/sec) = 313330 yeildtim (ns) 990
-----------------------------------------------------
Default internal MAIN
--------------- Application running ------------
  hi pri cnt = 1
  hi pri cnt = 1
  hi pri cnt = 2
  hi pri cnt = 2
  hi pri cnt = 3
  hi pri cnt = 3
  hi pri cnt = 4
  hi pri cnt = 4
  hi pri cnt = 5
  hi pri cnt = 5
  hi pri cnt = 6
  hi pri cnt = 6
  hi pri cnt = 7
  hi pri cnt = 7
  hi pri cnt = 8
  hi pri cnt = 8
  hi pri cnt = 9
  hi pri cnt = 9
  hi pri cnt = 10
hw_resetAndReboot() -> exit
