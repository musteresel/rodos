RODOS RODOS-200.0 OS Version Linux-x86
Loaded Applications:
         10 -> 'Topics & Middleware'
       1000 -> 'resumefromEvent'
Calling Initiators and Application Initiators
Distribute Subscribers to Topics
List of Middleware Topics:
 CharInput  Id = 28449 len = 16.   -- Subscribers:
 SigTermInterrupt  Id = 16716 len = 4.   -- Subscribers:
 UartInterrupt  Id = 15678 len = 4.   -- Subscribers:
 TimerInterrupt  Id = 25697 len = 4.   -- Subscribers:
 routerTopic  Id = 21324 len = 1326.   -- Subscribers:
 gatewayTopic  Id = 0 len = 16.   -- Subscribers:

Event servers:
	1 TimeEvent managers
Threads in System:
   Prio =       0 Stack =  32000 IdleThread: yields all the time
   Prio =     100 Stack =  32000 AnonymThread: 
BigEndianity = 0, cpu-Arc = x86, Basis-Os = baremetal, Cpu-Speed (K-Loops/sec) = 313330 yeildtim (ns) 990
-----------------------------------------------------
Default internal MAIN
--------------- Application running ------------
Suspend and wait until some one resumes me
   Time Event at   0.100000000
   Testwaiter resumed from me
testwaiter running again at   0.200000000
Suspend and wait until some one resumes me
   Time Event at   0.300000000
   Testwaiter resumed from me
testwaiter running again at   0.400000000
Suspend and wait until some one resumes me
   Time Event at   0.500000000
   Testwaiter resumed from me
testwaiter running again at   0.600000000
Suspend and wait until some one resumes me
   Time Event at   0.700000000
   Testwaiter resumed from me
testwaiter running again at   0.800000000
Suspend and wait until some one resumes me
   Time Event at   0.900000000
   Testwaiter resumed from me
testwaiter running again at   1.000000000
Suspend and wait until some one resumes me
   Time Event at   1.100000000
   Testwaiter resumed from me
testwaiter running again at   1.199999999
Suspend and wait until some one resumes me
   Time Event at   1.300000000
   Testwaiter resumed from me
testwaiter running again at   1.399999999
Suspend and wait until some one resumes me
   Time Event at   1.500000000
   Testwaiter resumed from me
testwaiter running again at   1.600000000
Suspend and wait until some one resumes me
   Time Event at   1.700000000
   Testwaiter resumed from me
testwaiter running again at   1.800000000
Suspend and wait until some one resumes me
   Time Event at   1.900000000
   Testwaiter resumed from me
testwaiter running again at   2.000000000
hw_resetAndReboot() -> exit
